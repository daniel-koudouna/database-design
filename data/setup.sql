drop database dzk_cs3101_db;

create database dzk_cs3101_db;

grant all on `dzk\_cs3101\_db`.* to 'kt54'@'%';
grant all on `dzk\_cs3101\_db`.* to 'dcchb'@'%';

use dzk_cs3101_db;

/* Database Schema */
create table restaurant_type(
type_id varchar(255),
primary key (type_id));

create table restaurant(
restaurant_id int(5) zerofill auto_increment,
restaurant_name varchar(255),
addr_street varchar(255) not null,
addr_city varchar(255) not null,
addr_postcode varchar(255) not null,
phone_number varchar(255) not null,
type_id varchar(255),
primary key (restaurant_id),
foreign key (type_id) references restaurant_type(type_id)
	on update cascade on delete set null);

create table item(
item_id char(16),
restaurant_id int(5) zerofill auto_increment,
name varchar(255) not null,
description varchar(255) not null default 'No description available',
price numeric(4,2) not null,
primary key(item_id),
foreign key (restaurant_id) references restaurant(restaurant_id)
	on update cascade on delete cascade);

create table dish_ingredients(
item_id char(16),
ingredient_name varchar(255) not null,
primary key(item_id, ingredient_name),
foreign key (item_id) references item(item_id)
	on update cascade on delete cascade);

create table preference(
preference_id varchar(255),
primary key (preference_id));

create table dish (
item_id char(16),
preference_id varchar(255) default 'none',
primary key (item_id, preference_id),
foreign key (item_id) references item(item_id)
	on update cascade on delete cascade,
foreign key (preference_id) references preference(preference_id)
	on update cascade);

create table drink (
item_id char(16),
is_alcoholic boolean not null default false,
primary key (item_id),
foreign key (item_id) references item(item_id)
	on update cascade on delete cascade);

create table courier (
courier_id int(5) zerofill auto_increment,
first_name varchar(255) not null,
last_name varchar(255) not null,
addr_street varchar(255) not null,
addr_city varchar(255) not null,
addr_postcode varchar(255) not null,
phone_number varchar(255) not null,
primary key (courier_id));

create table customer(
customer_id int(8) zerofill auto_increment,
first_name varchar(255) not null,
last_name varchar(255) not null,
addr_city varchar(255) not null,
addr_street varchar(255) not null,
addr_postcode varchar(255) not null,
email varchar(255) not null,
preference_id varchar(255) not null default 'none',
primary key (customer_id),
foreign key (preference_id) references preference(preference_id)
	on update cascade);

create table customer_phones(
customer_id int(8) zerofill not null,
phone_number varchar(255) not null,
primary key (customer_id, phone_number),
foreign key (customer_id) references customer(customer_id)
	on update cascade on delete cascade);

/* 
 * Orders should never be deleted from the database, as they mark all the
 * monetary transactions taking place. Therefore, even if some relevant data
 * is deleted, the fields should be nulled instead of a cascaded deletion.
 */
create table orders(
order_id int(10) zerofill auto_increment,
customer_id int(8) zerofill,
restaurant_id int(5) zerofill,
courier_id int(5) zerofill,
total_price numeric(5,2) not null default 0.0,
time_placed datetime not null default current_timestamp,
time_delivered datetime,
primary key (order_id),
foreign key (customer_id) references customer(customer_id)
	on update cascade on delete set null,
foreign key (restaurant_id) references restaurant(restaurant_id)
	on update cascade on delete set null,
foreign key (courier_id) references courier(courier_id)
	on update cascade on delete set null);

create table order_items(
order_id int(10) zerofill not null,
item_id char(16) not null,
quantity int not null default 1,
primary key(order_id, item_id),
foreign key (order_id) references orders(order_id)
	on update cascade on delete cascade,
foreign key (item_id) references item(item_id)
	on update cascade on delete cascade);

/* Procedures */

create procedure insert_dish(
item_id char(16),
restaurant_id int(5) zerofill,
name varchar(255),
description varchar(255),
price numeric(4,2),
preference_id char(16))
begin
start transaction;
insert into item values (item_id, restaurant_id, name, description, price);
insert into dish values (item_id, preference_id);
commit;
end;

create procedure insert_drink(
item_id char(16),
restaurant_id int(5) zerofill,
name varchar(255),
description varchar(255),
price numeric(4,2),
is_alcoholic boolean)
begin
start transaction;
insert into item values (item_id, restaurant_id, name, description, price);
insert into drink values (item_id, is_alcoholic);
commit;
end;


create procedure add_customer(
first_name varchar(255),
last_name varchar(255),
addr_street varchar(255),
addr_city varchar(255),
addr_postcode varchar(255),
email varchar(255),
preference_id varchar(255),
phone_number varchar(255))
begin
start transaction;
insert into customer (first_name, last_name, addr_city, addr_street, addr_postcode, email, preference_id) values
(first_name, last_name, addr_city, addr_street, addr_postcode, email, preference_id);
insert into customer_phones values
(last_insert_id(), phone_number);
commit;
end;

create procedure add_order_item(
order_id char(64),
item_id char(16),
quantity int)
begin

declare item_price numeric(4,2);
select price into item_price from item where item.item_id=item_id;
start transaction;

insert into order_items values (order_id, item_id, quantity);
update orders
set total_price = total_price + item_price*quantity
where orders.order_id = order_id;

commit;
end;

create procedure show_order(order_id int(8) zerofill)
begin
select item.name, item.description, order_items.quantity, item.price, (order_items.quantity*item.price) as total_price
from order_items natural join item
where order_items.order_id=order_id;
end;

create procedure show_items(
restaurant_id int(5) zerofill)
begin
select * from all_items I where I.restaurant_id=restaurant_id;
end;

/*
 * The method couriers would have access to, to complete orders when they
 * are successfully delivered.
 */
create procedure mark_order_delivered(
order_id int(8) zerofill)
begin
update orders O set time_delivered=current_timestamp
where O.order_id=order_id;
end;

/*
 * A debug method that should not be used under normal circumstances.
 * Allows setting the delivery time explicitly, which could skew
 * the statistics of the company.
 * Could conceivably be used to correct erronous orders.
 */
create procedure debug_mark_order_delivered(
order_id int(5) zerofill,
delivered datetime)
begin
update orders O set time_delivered=delivered
where O.order_id=order_id;
end;

/* Views & Queries */

/* View all current (undelivered) orders, with relevant attributes for delivery. */
create view current_orders as
select O.order_id, O.restaurant_id,
concat(R.addr_street," ", R.addr_city, ", ", R.addr_postcode) as "restaurant_address",
O.total_price, O.time_placed, O.customer_id, O.courier_id,
concat(C.addr_street, " ", C.addr_city, ", ", C.addr_postcode) as "delivery_address"
from orders O natural join customer C join restaurant R on O.restaurant_id=R.restaurant_id
where time_delivered is null;

/* View all items sold along with all the information, with null fields. */
create view all_items as
select I.item_id, I.restaurant_id, I.name, I.description, I.price, D.preference_id, Dr.is_alcoholic

from (item I left join dish D on I.item_id=D.item_id left join drink Dr on I.item_id=Dr.item_id);

/* View completed orders, with relevant information regarding to delivery time and price. */
create view completed_orders as
select O.order_id, O.restaurant_id, O.total_price, O.time_placed,
timestampdiff(minute, O.time_placed, O.time_delivered) as "time_taken", O.customer_id, O.courier_id
from orders O natural join customer C join restaurant R on O.restaurant_id=R.restaurant_id
where time_delivered is not null;

/* Q1: View the names of dishes that have not yet been ordered. */
create view q1 as
select item.name from item natural join dish D
where not exists (select * from order_items O where O.item_id=D.item_id);

/* Q2: Return the average delivery time between St. Andrews and Cupars. */
create view q2 as
select avg(O.time_taken) as 'average_time_standrews_cupars'
from completed_orders O natural join restaurant R join customer C on C.customer_id=O.customer_id
where R.addr_city like "St Andrews" and C.addr_city like "Cupars";

/* Q3: List the five most successful drinks in terms of money spent. */
create view q3 as
select O.item_id, I.name, sum(O.quantity)*I.price as "total_sale"
from order_items O natural join drink natural join item I
group by O.item_id order by total_sale desc limit 5;

/* List the most popular dishes for each restaurant.*/
create view most_popular_dishes as
select I.restaurant_id, I.item_id, I.name, max(O.quantity*I.price) as 'total_sale'
from order_items O natural join dish natural join item I
group by I.restaurant_id;

/* List the most popular restaurants in terms of total orders. */
create view most_popular_restaurants as
select R.restaurant_id, R.restaurant_name, count(*) as 'total_orders'
from orders O natural join restaurant R
group by O.restaurant_id;

/* Q4: List the most popular restaurants in terms of total orders, and the most popular dish of each. */
create view q4 as
select R.*, D.name as 'most_popular_dish', D.total_sale
from most_popular_restaurants R left join most_popular_dishes D on R.restaurant_id=D.restaurant_id
order by R.total_orders desc;

/* View the courier table along with the average delivery time for each courier. */
create view courier_averages as
select C.*, avg(O.time_taken) as 'average_time'
from completed_orders O natural join courier C group by C.courier_id;

/* Q5: List the fastest couriers for each town, by average delivery time. */
create view q5 as
select C.addr_city as 'city', C.courier_id, C.first_name, C.last_name, min(average_time) as 'average_time'
from courier_averages C
group by addr_city;

/* Triggers */

/* An order can only contain items from a single restaurant. */
create trigger order_items_bins before insert on order_items for each row
begin
declare r_id int(5) zerofill;
select (restaurant_id) into r_id from orders O where O.order_id=new.order_id;
if not exists ( select * from item I where I.item_id = new.item_id and I.restaurant_id=r_id ) then
   signal sqlstate '45001' set message_text='Cannot add item, does not belong to restaurant';
end if;
end;

/*
 * The database must contain at least one phone number for each customer.
 * Before a phone number is deleted, check that there is at least one more
 * number for that customer.
 */
create trigger customer_phones_bdel before delete on customer_phones for each row
begin
declare phones int;
 
if exists (select * from customer C where C.customer_id=old.customer_id) then
   select count(*) into phones from customer_phones C where C.customer_id=old.customer_id;
   if (phones = 1) then
      signal sqlstate '45001' set message_text='Cannot delete the only phone number';
   end if;
end if;

end;

/* Dish ingredients can only contain items which are dishes. */
create trigger dish_ingredients_bins before insert on dish_ingredients for each row
begin
if not exists (select * from dish D where D.item_id=new.item_id) then
   signal sqlstate '45001' set message_text='Cannot add ingredient, item does not exist or is not dish.';
end if;
end;

/* Dummy Data */

insert into restaurant_type (type_id) values
("Italian"),
("Mexican"),
("Chinese"),
("Burger"),
("Japanese");

insert into restaurant (restaurant_name, addr_street, addr_city, addr_postcode,phone_number, type_id) values
("Nandos","73 Market Street", "St Andrews", "KY16 9NU", "08001213", "Mexican"),
("Burger King","Wellgate Shopping Centre", "Dundee", "DD1 2DB", "08001255", "Burger");
("McDonalds","Longtown Rd", "Dundee", "DD4 8JT", "08009908", "Burger"),
("Dominos", "54 Market Steet", "St Andrews", "KY16 9NT", "08001130", "Italian");

insert into preference (preference_id) values
("none"),
("halal"),
("kosher"),
("vegeterian"),
("vegan");

call add_customer("Kasim", "Terzic", "63 Golf Street", "St Andrews", "KY14 4YF", "kt54@st-andrews.ac.uk", "none","0880123456");
call add_customer("David", "Harris", "74 Golf Street", "Dundee", "KY15 8FC", "dcchb@st-andrews.ac.uk", "none", "08003231");
call add_customer("Alexander", "Voss", "72 Quiet Street", "Cupars", "KY21 3F2", "alex@voss.de", "none","076548809");
call add_customer("Alexander", "Bain", "29 North Haugh", "St Andrews", "KY17 4BD", "alex@mail.com", "vegeterian","07009090");
call add_customer("David", "Harris", "30 North Haugh", "Cupars", "KF20 9GH", "david@email.com", "none","077612341");
call add_customer("Steve", "Linton", "10 Andrews Way", "St Andrews", "KY19 30D", "steve@cs.st-andrews.ac.uk", "none","08007777");

insert into customer_phones (customer_id, phone_number) values
(1, "08001289"),
(1, "02037477");

call insert_dish('MDB1', 1, "Big Mac", "Big and Tasty", 1.20, "none");
call insert_drink('MDD1', 1, "McFlurry", "Ice Cream Milkshake", 1.40, false);
call insert_dish('NDS1', 2, "Lime Chicken", "Mild chicken in zesty lime sauce", 4.20, "none");
call insert_dish('NDS2', 2,"Chilli Chicken", "Spicy Chicken in tangy chilli sauce", 4.10, "none");
call insert_dish('BK01', 3,"Whopper", "Classic burger", 2.80, "none");
call insert_dish('BKD1', 3,"Double Whopper", "Classic double burger", 3.40, "none");
call insert_dish('BKD3', 3,"Soya Whopper", "Classic vegeterian dish", 2.70, "vegeterian");
call insert_drink('DMN1',4,"Mineral Water", "What more do you need?", 1.20, false);
call insert_drink('DMN2',4,"Pepsi", "Classic fizzy drink", 0.80, false);
call insert_drink('BKD4',3,"Wine", "1998", 6.10, true);
call insert_drink('NDS3',2,"Chocolate Smoothie", "With chicken", 2.40, false);
call insert_drink('NDS4',2,"Sprite", "Without chicken", 1.50, false);
call insert_drink('BKD5',3,"Diet Coke", "With extra calories", 0.70, false);
call insert_dish('DMN3', 4,"Large Pizza", "With extra calories", 5.20, 'none');
call insert_dish('DMN4',4,'Hawaiian Pizza', 'Suitable for vegeterians', 5.40, 'vegeterian');

insert into dish_ingredients(item_id, ingredient_name) values
('MDB1', 'Beef'),
('MDB1', 'Lettuce'),
('NDS1', 'Chicken'),
('NDS1','Lime'),
('NDS2','Chicken'),
('NDS2','Chilli Power'),
('BK01','Angus Beef'),
('DMN3','Mozarella');

insert into courier
(first_name, last_name, addr_street, addr_city, addr_postcode, phone_number) values
("Tom", "Kelsey", "12 Data Avenue", "St Andrews", "KY16 42D", "08008785"),
("Mark", "Nederhof", "15 Data Avenue", "St Andrews", "KY20 30F", "9009018"),
("Chris", "Jefferson", "40 Game Lane", "Dundee", "KR23 5FF", "800787878");

insert into orders
(customer_id, restaurant_id, courier_id) values
(2,1,1),
(3,3,2),
(5,4,3),
(5,4,3),
(3,4,1),
(3,4,3),
(4,2,2);

call add_order_item(1,'MDB1',3);
call add_order_item(1,'MDD1',2);

call add_order_item(2,'BKD1',1);
call add_order_item(2,'BKD4',10);
call add_order_item(2,'BKD3',2);


call add_order_item(3,'DMN1',5);
call add_order_item(3,'DMN2',7);

call add_order_item(4,'DMN1',3);
call add_order_item(4,'DMN2',4);

call add_order_item(5,'DMN3',2);
call add_order_item(5,'DMN4',1);
call add_order_item(5,'DMN1',5);
call add_order_item(5,'DMN2',2);

call add_order_item(6,'DMN4',3);
call add_order_item(6,'DMN3',2);
call add_order_item(7,'NDS4',2);
call add_order_item(7,'NDS3',1);

call debug_mark_order_delivered(1, timestampadd(minute,15,current_timestamp));
call debug_mark_order_delivered(2, timestampadd(minute,22,current_timestamp));
call debug_mark_order_delivered(3, timestampadd(minute,18,current_timestamp));
call debug_mark_order_delivered(4, timestampadd(minute,20,current_timestamp));
call debug_mark_order_delivered(5, timestampadd(minute,19,current_timestamp));
