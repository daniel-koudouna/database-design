% Packages
\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage[usenames,dvipsnames]{color} % Required for custom colors
\usepackage{graphicx} % Required to insert images
\usepackage{listings} % Required for insertion of code
\usepackage{courier} % Required for the courier font
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{float}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass\ : \hmwkTitle} % Top center head
\rhead{\firstxmark} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \protect\pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

% Code
\lstdefinestyle{code}{
	backgroundcolor=\color{white},   
	commentstyle=\color{DarkOrchid},
	keywordstyle=\color{MidnightBlue},
	numberstyle=\tiny\color{Blue},
	stringstyle=\color{Salmon},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}

\lstset{style=code}

\newcommand{\dbtable}[2]{
\begin{figure}[H]
	\centering
	\includegraphics[height=0.2\textheight, width=\textwidth, keepaspectratio]{#1}
	\caption{#2}
\end{figure}
}

% Details
\newcommand{\hmwkTitle}{Practical 1 - Database Design} % Assignment title
\newcommand{\hmwkClass}{CS3101} % Course/class
\newcommand{\hmwkAuthorName}{dzk - 140 000 346} % Your name

% Title
\title{
\vspace{2in}
\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
\normalsize\vspace{0.1in}\small{\today}\\
}

\author{\textbf{\hmwkAuthorName}}
\date{}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle

\newpage

\tableofcontents

\newpage

\section{Overview}
In this practical, I was asked to design a database system for a delivery service using an E-R model, and implement it using a MariaDB database, by translating the E-R model into a relational schema. In addition, I demonstrated how my database functions by adding some dummy data to the it, and executing relatively complex queries.

\section{Runtime}
During development, I used an \lstinline|.sql| file which contained the database schema, including privileges, procedures and dummy data. The file drops the current database and re-creates it. This allowed me to quickly make changes and find errors in the schema. The file is included as \lstinline|setup.sql| and can be run on any mariaDB server. 

The live database can be found at \lstinline|dzk.host.cs.st-andrews.ac.uk| as \lstinline|dzk_cs3101_db|

\section{Design}

\subsection{E-R Model}

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{../data/db_design.png}
	\caption{The E-R model. Note that due to limitations of the drawing package used, a bold line is used to indicate total participation.}
	\label{fig:model}
\end{figure}

I tried to keep the E-R Model as simple as possible, while allowing for maximum flexibility. The main entity sets have a respective ID, which is used as the primary key. This makes the database design consistent, since all primary keys could share a common data type, and thus be recognized easily. In addition, this exposes a simpler API to the database users, explored more in section \ref{sec:sql}.

The Order entity set is particularly interesting, since it fully participates in four relationships, one with each of the main tables. In addition, it has a One-to-Many relationship with Customer, Restaurant and Courier. A consequence of this is that a single order must only reference a single restaurant. I did consider a more flexible system, where a customer can order food from two different restaurants at once, but ultimately decided against it. multiple restaurants in the Services relationship would also complicate the Delivers relationship, as potentially more than one courier would be needed to deliver the order.

In regard to the Item entity set, I decided to use inheritance to create the subclasses for the Dish and Drink entity sets. This is a total, disjoint inheritance, as each item is either a dish or a drink. In addition, I added a preference field in dishes, which could be used to distinguish between vegetarian, vegan and other types of dishes. 

Another notable feature of my model is that I chose to not extract addresses into a separate entity set. The address details always need to be queried in addition to their respective entity sets, and never on their own. For example, an order needs to access the customer and their address at the same time. Therefore, there is no need to split the data and introduce another table query, which would increase space and time requirements. Finally, adding an address entity set would make it difficult to manipulate the respective address table, since it would contain information for three other tables.

\subsection{Relational Model}

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{../data/db_schema.png}
	\caption{The Relational model.}
	\label{fig:relational}
\end{figure}

When converting my E-R model into a relational schema, I made a few changes to make the database more consistent. I added a table specifically for the restaurant type (i.e Mexican, Chinese etc.). I believe that the types of restaurants would want to be closely regulated, either to keep consistent with the franchise's naming, or even to make it easier to display on the front-end. 

In addition, I added a table specifically for the dietary preference. This is referenced by both the customer and the dish table, which allows the user to easily query which restaurant offers the greatest variety of food for their specific dietary preference. This is aided by the fact that I split dishes and drinks into subclasses of items, which means a natural join on dishes will never introduce null fields. 

I considered using enums for both preference and restaurant type, but, after some research, it seems MariaDB handles enums is not very well implemented and differs slightly from SQL. Therefore I decided to follow the above approach for the sake of simplicity and extensibility, as this schema could conceivably be used with a MySQL server instead.

As expected, I created a separate table for multi-valued attributes to easily bring the database at least to first normal form, shown in tables \lstinline|customer_phones| and \lstinline|dish_ingredients|. In addition, since three of the relationships on orders in the E-R model were of total participation, and also many-to-one on the side of orders, it was easy to replace them with fields inside the order table itself. This reduces complexity and makes the database much easier to work with, especially since most queries will be pertaining to the orders table. 
\section{SQL Schema} \label{sec:sql}

The restaurant\_type table has no special qualities, and is used to store restaurant categories with more power than an enum.
\lstinputlisting[language=SQL,firstline=11,lastline=13]{../data/setup.sql}

The restaurant table references restaurant\_type. On the rare occasion that a category is deleted, the field can safely be set to null. A default restaurant category was considered, but is not very worthwhile, as usually after a category is deleted, the affected restaurants will be re-categorized.
\lstinputlisting[language=SQL,firstline=15,lastline=25]{../data/setup.sql}

The item table uses a string as the primary key, as different restaurants may use strings to identify items their products. This might make it easier to reference which products were sold, for example for administration purposes. However, it seems fair that if a restaurant is deleted from the database entirely, all items of that restaurant be deleted as well.
\lstinputlisting[language=SQL,firstline=27,lastline=35]{../data/setup.sql}

The item\_ingredients table uses a joint primary key, and is deleted when the corresponding item is deleted. Ingredients are stored simply as strings.
\lstinputlisting[language=SQL,firstline=37,lastline=42]{../data/setup.sql}

The preference table simply holds a list of preferences, another enum-like table.
\lstinputlisting[language=SQL,firstline=44,lastline=46]{../data/setup.sql}

The dish table showcases a weakness of the MariaDB database. I initially wanted to use \lstinline|on delete set default|, which is supported in MySQL but not MariaDB. However, 
this can easily be re-implemented with a trigger. Regarding to item, the subclass is deleted when the item is deleted.
\lstinputlisting[language=SQL,firstline=48,lastline=55]{../data/setup.sql}

The drink table closely resembles the dish table, with similar reasoning. 
\lstinputlisting[language=SQL,firstline=57,lastline=62]{../data/setup.sql}

The courier table has no special properties.
\lstinputlisting[language=SQL,firstline=64,lastline=72]{../data/setup.sql}

The customer table, once again, should set the preference to the default value if it is deleted.
\lstinputlisting[language=SQL,firstline=74,lastline=85]{../data/setup.sql}

The list of customer phones relies on the customer, and should be deleted if the customer is deleted.
\lstinputlisting[language=SQL,firstline=87,lastline=92]{../data/setup.sql}

Since the orders table acts as a monetary record of the company, orders should usually never be deleted. Therefore, the foreign keys of the table could be set to null if the other entities are deleted, but the core data, like the total price should always be retained, for example for tax reasons.
\lstinputlisting[language=SQL,firstline=99,lastline=113]{../data/setup.sql}

The order items table stores the collection of items in each order, including their quantities. If the order is somehow deleted, the entry should also be deleted, as it is no longer useful. 
\lstinputlisting[language=SQL,firstline=115,lastline=123]{../data/setup.sql}

\subsection{Procedures}
I also included a number of procedures to expose a simple API for users to carry out the most frequent database queries. In the interest of space, they can be seen in the appendix in section \ref{sec:sqldump}. 

I included procedures for adding dishes and drinks, without the need to first create an item for the corresponding restaurant, and then either a dish or drink. This also satisfies the integrity constraint of the generalization, since, by using this procedure instead of direct access to the tables, either a drink or dish will always be created. 

I added a procedure to add a customer, which also registers the customer's phone in the separate table, without the need to call 2 database insert methods. In addition, the procedures I created do not usually reference primary keys, as they are generated automatically, since their data type is \lstinline|auto_increment|.

In addition, I added a number of procedures to access the orders table. Firstly, I created a procedure to add an item to an order, which will not only add the item, but also update the total price of the order to reflect the new total. This happens in a single transaction, which eliminates and errors or inconsistencies that might arise if a user would manually execute both queries, and have one of them fail. I also added procedures to mark an order as delivered, which completes an order without the need for the user to specify the current timestamp. This eliminates any inconsistencies and increases usability. 

Finally I added query procedures to show a restaurant's items for a given restaurant, or to show a given order, which I would expect to be heavily used in a real world scenario, for both the front-end and admin purposes.

\subsection{Triggers}
I added some triggers to satisfy more integrity constraints that could not be implemented with only the schema. One of my triggers checks that every item added to an order is from the restaurant that was specified when creating the order. In other words, each order may only contain items from a single restaurant. This could have also been implemented with a check function, but MariaDB does not yet support it. In addition, I created a trigger to check that a customer always has as least one phone number available. This means that the final phone number of a customer can never be deleted, unless the customer themselves are deleted from the database.

\subsection{Queries \& Views}
I created a variety of views to help organize and aggregate data, which I used in both procedures and queries. Beyond the required queries, I added views for completed and uncompleted orders, each with its own columns. For example, completed orders show the time taken and the total price of the order, while current orders show the restaurant and delivery address. This has many potential use cases, such as an admin panel showing all current orders, and could potentially be used as a materialized view for faster querying. In addition, the completed orders view simplified the implementation of other, more complicated views. All the views can be found in section \ref{sec:sqldump}, and examples can be seen with data in \ref{sec:data}.


\section{Conclusion}
In conclusion, I think I have created a consistent and easy to use database. I implemented various triggers, procedures and views to simplify access to the database and tried to handle integrity constraints under the hood by using procedures as an API for accessing the database for complex operations. Finally, I think I executed the required queries in an elegant way, with the help of views and my database design, and also created additional queries which will be useful in the long term use an maintenance of the database.

\section{Appendix}

\subsection{Database Tables, Queries \& Data} \label{sec:data}

\subsubsection{Tables}
\dbtable{../data/t_courier.png}{The courier table}

\dbtable{../data/t_restaurant.png}{The restaurant table}
\dbtable{../data/t_restaurant_type.png}{The restaurant\_type table}
\dbtable{../data/t_customer.png}{The customer table}
\dbtable{../data/t_customer_phones.png}{The customer\_phones table}
\dbtable{../data/t_preference.png}{The preference table}

\dbtable{../data/t_orders.png}{The orders table}
\dbtable{../data/t_item.png}{The item table}
\dbtable{../data/t_dish.png}{The dish table}
\dbtable{../data/t_dish_ingredients.png}{The dish\_ingredients table}

\dbtable{../data/t_drink.png}{The drink table}

\dbtable{../data/t_order_items.png}{The order\_items table}

\subsubsection{Queries}
\dbtable{../data/t_q1.png}{The view for Q1, the dishes never ordered}

\dbtable{../data/t_q2.png}{The view for Q2, the average delivery time}

\dbtable{../data/t_q3.png}{The view for Q3, the five most popular drinks}

\dbtable{../data/t_q4.png}{The view for Q4, the most popular restaurants with their most popular dish. Included is an example of a restaurant with no dishes ordered}

\dbtable{../data/t_q5.png}{The view for Q5, the fastest couriers for each city}

\dbtable{../data/t_current_orders.png}{The view for orders not yet delivered}

\dbtable{../data/t_completed_orders.png}{The view for completed orders}

\subsection{Database Setup Code} \label{sec:sqldump}
\lstinputlisting[language=SQL]{../data/setup.sql}

\end{document}