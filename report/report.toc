\contentsline {section}{\numberline {1}Overview}{3}
\contentsline {section}{\numberline {2}Runtime}{3}
\contentsline {section}{\numberline {3}Design}{4}
\contentsline {subsection}{\numberline {3.1}E-R Model}{4}
\contentsline {subsection}{\numberline {3.2}Relational Model}{5}
\contentsline {section}{\numberline {4}SQL Schema}{6}
\contentsline {subsection}{\numberline {4.1}Procedures}{8}
\contentsline {subsection}{\numberline {4.2}Triggers}{9}
\contentsline {subsection}{\numberline {4.3}Queries \& Views}{9}
\contentsline {section}{\numberline {5}Conclusion}{9}
\contentsline {section}{\numberline {6}Appendix}{9}
\contentsline {subsection}{\numberline {6.1}Database Tables, Queries \& Data}{9}
\contentsline {subsubsection}{\numberline {6.1.1}Tables}{9}
\contentsline {subsubsection}{\numberline {6.1.2}Queries}{13}
\contentsline {subsection}{\numberline {6.2}Database Setup Code}{14}
